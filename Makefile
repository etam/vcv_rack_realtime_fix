CC=gcc
CFLAGS=-fPIC

all: rt_fix.so

rt_fix.so: rt_fix.o
	$(CC) -shared -fPIC $^ -o $@ -ldl

clean:
	$(RM) *.o *.so

.PHONY: all clean
