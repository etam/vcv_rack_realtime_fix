#define _GNU_SOURCE
#include <dlfcn.h>
#include <pthread.h>
#include <sched.h>

int pthread_setschedparam(pthread_t thread, int policy, const struct sched_param* param)
{
    typedef int (*pthread_setschedparam_t)(pthread_t, int, const struct sched_param*);
    pthread_setschedparam_t real_pthread_setschedparam = dlsym(RTLD_NEXT, "pthread_setschedparam");

    if (policy == 0 && param->sched_priority == 0) {
        return 0;
    }

    return real_pthread_setschedparam(thread, policy, param);
}

int sched_get_priority_max(int policy)
{
    typedef int (*sched_get_priority_max_t)(int);
    sched_get_priority_max_t real_sched_get_priority_max = dlsym(RTLD_NEXT, "sched_get_priority_max");

    if (policy == SCHED_RR) {
        return 95;
    }

    return real_sched_get_priority_max(policy);
}
