# Workaround for VCV Rack crash

## Building

```shell
make
```

## Usage

```shell
cd /path/to/Rack
env LD_PRELOAD=/path/to/vcv_rack_realtime_fix/rt_fix.so ./Rack
```
